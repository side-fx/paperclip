# Paperclip

Command line utility written in rust to fetch download links for [PaperMC]() server binaries.
I built this to use in docker build scripts for my own private minecraft servers.

## Usage

Paperclip takes a single argument, the minecraft version you would like your server to run.
This can be a specific patch
```
> paperclip 1.18.2
```

or a minor version

```
> paperclip 1.19
```

## Plans

- Command line flag to download the binary instead of just printing a link to the download
