use anyhow::{Error, Result};

pub const PAPER_BASE_URL: &'static str = "https://api.papermc.io";
pub const PAPER_PROJECT: &'static str = "paper";

pub const URL_TEMPLATE: &'static str =
    "https://api.papermc.io/v2/projects/paper/versions/{version}/builds/{build}/downloads/paper-{version}-{build}.jar";

pub enum Target {
    Latest,
    Group(VersionGroup),
    Version(McVersion),
}

impl Target {
    pub fn new(text: String) -> Self {
        if let Ok(version) = try_parse_version(text) {
            return version.into();
        }
        Target::Latest
    }
}

pub struct VersionGroup {
    major: u8,
    minor: u8,
}

impl ToString for VersionGroup {
    fn to_string(&self) -> String {
        format!("{}.{}", self.major, self.minor)
    }
}

pub struct McVersion {
    major: u8,
    minor: u8,
    patch: u8,
}

impl ToString for McVersion {
    fn to_string(&self) -> String {
        format!("{}.{}.{}", self.major, self.minor, self.patch)
    }
}

enum ParsedVersion {
    Group(u8, u8),
    Version(u8, u8, u8),
}

impl Into<Target> for ParsedVersion {
    fn into(self) -> Target {
        match self {
            ParsedVersion::Group(major, minor) => Target::Group(VersionGroup { major, minor }),
            ParsedVersion::Version(major, minor, patch) => Target::Version(McVersion {
                major,
                minor,
                patch,
            }),
        }
    }
}

fn try_parse_version(text: String) -> Result<ParsedVersion> {
    let values = text
        .split('.')
        .into_iter()
        // .map(|x| x.to_owned())
        .map(|x| u8::from_str_radix(&x, 10));

    match values.collect::<Vec<_>>().as_slice() {
        [Ok(major), Ok(minor)] => Ok(ParsedVersion::Group(*major, *minor)),
        [Ok(major), Ok(minor), Ok(patch)] => Ok(ParsedVersion::Version(*major, *minor, *patch)),
        _ => Err(Error::msg(format!("Failed to parse version from {text}"))),
    }
}
