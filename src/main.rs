use anyhow::Result;
use paper_mc::PaperMcClient;
use std::env;

use paperclip::*;

#[tokio::main]
async fn main() -> Result<()> {
    let client = PaperMcClient::new(&PAPER_BASE_URL);
    let args = env::args().collect::<Vec<_>>();
    let target = Target::new(args.last().unwrap().clone());

    let url = match target {
        Target::Latest => handle_latest(client).await,
        Target::Group(group) => handle_group(client, &group.to_string()).await,
        Target::Version(version) => handle_version(client, &version.to_string()).await,
    };

    if let Ok(link) = url {
        print!("{}", link);
    }

    Ok(())
}

async fn handle_latest(client: PaperMcClient) -> Result<String> {
    let groups = client
        .project(&PAPER_PROJECT)
        .send()
        .await?
        .version_groups
        .unwrap();
    let group = groups.last().unwrap();
    handle_group(client, group).await
}

async fn handle_group(client: PaperMcClient, group: &String) -> Result<String> {
    let versions = client
        .family(&PAPER_PROJECT, &group)
        .send()
        .await?
        .versions
        .unwrap();
    let version = versions.last().unwrap();

    handle_version(client, version).await
}

async fn handle_version(client: PaperMcClient, version: &String) -> Result<String> {
    let builds = client
        .builds(&PAPER_PROJECT, version)
        .send()
        .await?
        .builds
        .unwrap();
    let build = builds.last().unwrap().build.unwrap();

    let url = URL_TEMPLATE
        .replace("{version}", version)
        .replace("{build}", &build.to_string());

    Ok(url)
}
